import numpy as np
import matplotlib.pyplot as plt

# readFastaFile1
# PARAMETERS : the filename (the file is in fasta format)
# RETURNS : a unique string with all the protein sequences concatenated

def readFastaFile1(filename) :
    
    # opening the file whose name is filename
    fd = open(filename,'r')
    txt = fd.read()
    fd.close()
    
    # txt contains all the text of the file. 
    # fisrt, I want to seperate the proteins, the symbol that starts a new protein is '>'
    seqs = txt.split('>')[1:]
    s = ""
    
    for seq in seqs :
        lines = seq.split('\n')[1:]     
        for line in lines :
            s = s + line
    return(s)

# readFastaFile2
# PARAMETERS : the filename (the file is in fasta format)
# RETURNS : a list with all the protein sequences

def readFastaFile2(filename) :
    
    # opening the file whose name is filename
    fd = open(filename,'r')
    txt = fd.read()
    fd.close()
    
    # txt contains all the text of the file. 
    # fisrt, I want to seperate the proteins, the symbol that starts a new protein is '>'
    seqs = txt.split('>')[1:]
    listSeq = []
    
    for seq in seqs :
        lines = seq.split('\n')[1:]       
        s = ""  
        for line in lines :
            s = s + line
        listSeq.append(s)
    
    return(listSeq)

#s_fasta = readFastaFile1("small-example.fasta")
#print(s_fasta)
#l_fasta = readFastaFile2("small-example.fasta")
#print(l_fasta)
    
#####################################################################################
### Identify common English words in the small human proteome with readFastaFile1 ###
#####################################################################################
    
print("\n#### Identify common English words in the small human proteome with readFastaFile1 ####")

# Give a unique string with all the protein sequences concatenated
proteome_small = readFastaFile1("human-proteome-small.fasta")
# print(Proteome_small)

# Put all the letters in lowercase
proteome_small = proteome_small.lower()

print("Done listing human proteome sequences. Length: {0}.".format(len(proteome_small)))

# Listing English words in txt format
english_words = []

with open("english-words.txt", 'r') as words :
    
    for word in words.readlines() : # Return all lines in the file, as a list 
        english_words.append(word[:-1].lower())
        
print("Number of english words : {0} words.".format(len(english_words)))
#print(English_words)

# Search English words in the small human proteome

def search_words(english_words,proteome_small) :
    
    search_english_words = [] # Create a table 
    i = 0
    
    for words in english_words : # Whenever we find an English word in the small proteome, we add +1 to i
        i += 1 
        if words in proteome_small : 
            search_english_words.append(words)
    
    return search_english_words

### Function test ###

print("\n### FUNCTION TEST ###")

#Test 1 : In case there are no words in the sequence
print("\n# Test 1 : In case there are no words in the sequence")

words_test1 = ["butterflies","sun","food","fish","umbrella"] # Create a word list
sequence_test1 = "jsiofgearoffhzofidjskofnclvmpeusijebnhffj" # Create a sequence

function_test1 = search_words(words_test1,sequence_test1) # Function test

print("\nNumber of english words in test 1 : ")
print(len(function_test1)) 

# Test 2 : In case there are words in the sequence
print("\n# Test 2 : In case there are words in the sequence")

words_test2 = ["butterflies","sun","food","fish","umbrella"] # Create a word list
sequence_test2 = "jsiofgearsunhzofidjskofnclvmfishijebnhffj" # Create a sequence

print("\nEnglish words in test 2 : ")
function_test2 = search_words(words_test2,sequence_test2) # Function test
print(function_test2)

print("\nNumber of english words in test 2 : ")
print(len(function_test2)) 

# Test 3 : In case there are duplicate words in the sequence
print("\n# Test 3 : In case there are duplicate words in the sequence")

words_test3 = ["butterflies","sun","food","fish","umbrella"] # Create a word list
sequence_test3 = "jsunfgearsunhzofidjskofnclvmfishijebnhffj" # Create a sequence

print("\nEnglish words in test 3 : ")
function_test3 = search_words(words_test3,sequence_test3) # Function test
print(function_test3)

print("\nNumber of english words in test 3 : ")
print(len(function_test3)) 

print("\nThe function counts each word present in the sequence but not if they are represented several times.")

### Results ###

print("\n### RESULTS ###")

print("\nEnglish words in small proteome : ")
find_words = search_words(english_words,proteome_small)
print(find_words)

print("\nNumber of english words in small proteome : ")
print(len(find_words)) 

### Figure ###

print("\n### REALIZATION OF A FIGURE ###")

# Function to calculate the number of words starting with each letter of the alphabet
def count(first_letter) : 
    
    counters = [0] * 26  # Create a table with 26 cases
    
    for words in first_letter:
        num_premiere_lettre = ord(words[0]) - 97 # Give place in the alphabet of the first letter of the word
        counters[num_premiere_lettre] += 1 # Add 1 for each first letter found in the corresponding box of the table
    
    return(counters)
    
### Function test ###

print("\n## FUNCTION TEST ##")

# Function test 
print("\n# Test 1")

list_test1 = ["animals","apricot","butterflies","sun","food","fish","umbrella"] # Create a word list

function_test_count1 = count(list_test1) # Function test

print("\nNumber of words starting with each letter of the alphabet in the test list : ")
print(function_test_count1)

### Results ###

print("\n## RESULTS ##")

# Number of words starting whith each letter of the alphabet in list of English words
number_of_letters_tot = count(english_words)
print("\nNumber of words starting with each letter of the alphabet in list of English words :")
print(number_of_letters_tot)

# Number of words starting whith each letter of the alphabet in list of small human proteome
number_of_letters_proteome = count(find_words)
print("\nNumber of words starting with each letter of the alphabet in list of small human proteome :")
print(number_of_letters_proteome)

print("\n## GRAPHIC ##")

# Allows to have the letters represented on the x axis
bars = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')
x_bar = np.arange(len(bars)) # Return evenly spaced values 

ratio = [0] * 26 # Create a table with 26 cases

# Calculate the ratio between the number of words beginning with each letter 
# in the small proteome and in the list of English words
for I in range (26) :
    
    if number_of_letters_tot[I] != 0 : 
        ratio[I] = (number_of_letters_proteome[I] / number_of_letters_tot[I])

# Graphic

plt.bar(x_bar, ratio) # Create a graph
plt.title("Histogram representing the ratio of words beginning with each letter of the alphabet")
plt.xlabel("Letters of the alphabet")
plt.ylabel("Ratio")
plt.xticks(x_bar, bars) # Get or set the current tick locations and labels of the x-axis
plt.show()

print("\nThe graph shows us that some letters are never represented. There are no words beginning with the letters B, J, O, Q, U, X and Z in the human proteome. On the other hand, it can be seen that the words beginning with the letter K are the most frequent in the small proteome.")

###########################################################
### Identify common English words in the human proteome ###
###########################################################

print("\n#### Identify common English words in the human proteome with readFastaFile1 ####")

# Give a unique string with all the protein sequences concatenated
human_proteome = readFastaFile1("human-proteome.fasta")
# print(Proteome_small)

# Put all the letters in lowercase
human_proteome = human_proteome.lower()

print("Done listing human proteome sequences. Length: {0}.".format(len(human_proteome)))

# Search English words in the human proteome

print("\n### Search English words in the human proteome ###")

print("\nEnglish words in proteome : ")
find_words2 = search_words(english_words,human_proteome)
print(find_words2)

print("\nNumber of english words in proteome : ")
print(len(find_words2)) 

### Figure ###

print("\n### REALIZATION OF A FIGURE ###")

# Count function results 

print("\n## COUNT FUNCTION RESULTS ##")

# Number of words starting whith each letter of the alphabet in list of human proteome
number_of_letters_proteome2 = count(find_words2)
print("\nNumber of words starting with each letter of the alphabet in list of human proteome :")
print(number_of_letters_proteome2)

print("\n## GRAPHIC ##")

# Allows to have the letters represented on the x axis
bars = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')
x_bar = np.arange(len(bars)) # Return evenly spaced values 

ratio2 = [0] * 26 # Create a table with 26 cases

# Calculate the ratio between the number of words beginning with each letter 
# in the proteome and in the list of English words
for I in range (26) :
    
    if number_of_letters_tot[I] != 0 : 
        ratio2[I] = (number_of_letters_proteome2[I] / number_of_letters_tot[I])

# Graphic

plt.bar(x_bar, ratio2) # Create a graph
plt.title("Histogram representing the ratio of words beginning with each letter of the alphabet")
plt.xlabel("Letters of the alphabet")
plt.ylabel("Ratio")
plt.xticks(x_bar, bars) # Get or set the current tick locations and labels of the x-axis
plt.show()

print("\nThe graph shows us similar results as in the small sequence previously used. On the other hand this time the letter U is present at the beginning of certain words. Words beginning with the letter K are always the most present.")